call plug#begin('~/.config/nvim/plugged')
	Plug 'Shougo/deoplete.nvim'
	Plug 'deoplete-plugins/deoplete-jedi'
	Plug 'scrooloose/nerdtree'
	Plug 'nathanaelkane/vim-indent-guides'
	Plug 'morhetz/gruvbox'
	Plug 'pbondoer/vim-42header'
	Plug 'preservim/nerdtree'
call plug#end()


let g:python_host_prog = '/usr/bin/python'
let g:python3_host_prog = '/usr/local/bin/python3'
let g:deoplete#enable_at_startup = 1

set encoding=UTF-8
filetype plugin indent on
syntax on
set number

nnoremap <S-Up> :resize +2<CR> 
nnoremap <S-Down> :resize -2<CR>
nnoremap <S-Left> :vertical resize +2<CR>
nnoremap <S-Right> :vertical resize -2<CR>
nnoremap <C-j> :tabprevious<CR>
nnoremap <C-k> :tabnext <CR>
nnoremap <C-s> :split <CR>
nnoremap <C-n> :NERDTreeToggle <CR>
nnoremap <C-f> :NERDTreeFocus <CR>

set statusline=
set statusline+=%#IncSearch#
set statusline+=\ %y
set statusline+=\ %r
set statusline+=%#CursorLineNr#
set statusline+=\ %F
set statusline+=%= "Right side settings
set statusline+=%#Search#
set statusline+=\ %l/%L
set statusline+=\ [%c]

colorscheme gruvbox
set background=dark termguicolors cursorline
set ts=4 sw=4
set mouse=a
set matchpairs+=<:>,=:;,+:;
set showcmd
