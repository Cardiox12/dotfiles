#!/bin/sh

CONF_DIR="$HOME/.config/nvim"
CONF_FILE="./config/init.vim"

if [ ! -d "$CONF_DIR" ] ; then
	echo "$CONF_DIR doesn't exists, creating it."
	mkdir "$CONF_DIR" &>/dev/null
fi

echo "Copying $CONF_FILE into $CONF_DIR"
cp "$CONF_FILE" "$CONF_DIR"
echo "Installing python packages for deoplete"
python3 -m pip install pynvim msgpack-python &>/dev/null  
